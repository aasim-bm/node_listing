const postUserUrl = "http://localhost:3000/users/new";
const getAllUrl = " http://localhost:3000/users/all";

// ADD USER
function add() {
  // Get Form values
  const formData = $("form")
    .serializeArray()
    .reduce((obj, item) => ({ ...obj, ...{ [item.name]: item.value } }), {});

  // AJAX POST REQUEST
  $.ajax({
    type: "POST",
    url: postUserUrl,
    data: JSON.stringify(formData),
    success: function (data) {
      console.log("data: " + JSON.stringify(data));
      toastr.success("Record Saved");
      clearForm();
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json",
  });
}

function clearForm() {
  $("form").trigger("reset");
}
