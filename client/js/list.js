const getAllUrl = " http://localhost:3000/users/all";

const table = document.querySelector("table");
const rowTemp = document.querySelector("#row-template");

$(document).ready(function () {
  // GET LIST
  fetch(getAllUrl)
    .then((response) => response.json())
    .then((json) => {
      //console.log(json);
      showData(json);
    });
});

function showData(data) {
  data.forEach((user) => {
    const { name, dept, age } = user;

    const cloneRow = document.importNode(rowTemp.content, true);

    cloneRow.querySelector(".name").innerText = name;
    cloneRow.querySelector(".dept").innerText = dept;
    cloneRow.querySelector(".age").innerText = age;

    table.appendChild(cloneRow);
  });
}
