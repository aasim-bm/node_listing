import express from "express";

import { getUsers, createUser } from "../controllers/users.js";

const router = express.Router();

// GET ALL USERS
router.get("/all", getUsers);

// CREATE A USER
router.post("/new", createUser);

export default router;
