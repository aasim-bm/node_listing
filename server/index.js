import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";

import usersRoute from "./routes/users.js";

var app = express();

// MIDDLEWARES
app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use("/users", usersRoute);

const CONNECTION_URL =
  "mongodb+srv://asim:12345@cluster0.9hzvc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

const PORT = 3000;

mongoose
  .connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() =>
    app.listen(PORT, () =>
      console.log(`Server Running on Port: http://localhost:${PORT}`)
    )
  )
  .catch((error) => console.log(`${error} did not connect`));

mongoose.set("useFindAndModify", false);
